#include "gtest/gtest.h"
#include "..\CppCollections\list.h"
using Collections::List;

TEST(ListTests, AddInteger_EmptyIntList_SizeEquals1)
{
    List<int> testList;
    testList.push_back(5);
    
    EXPECT_EQ(testList.size(), 1) << "List size did not increase when value was added.";  
}

TEST(ListTests, AddInteger_EmptyIntList_CapacityIncreases)
{
    List<int> testList;
    testList.push_back(5);
    
    EXPECT_GT(testList.capacity(), 0) << "List capacity did not increase when value was added.";  
}

TEST(ListTests, RemoveValue_IntListOf5Values_SizeEquals4)
{
    List<int> testList(5);
    for (int i=0; i<5; ++i)
    {
        testList[i] = i;
    }
    
    testList.remove(2);
    
    EXPECT_EQ(testList.size(), 4) ;
}

TEST(ListTests, RemoveValue_IntListOf5Values_CapacityUnaffected)
{
    List<int> testList(5);
    for (int i=0; i<5; ++i)
    {
        testList[i] = i;
    }
    
    testList.remove(2);
    
    EXPECT_EQ(testList.capacity(), 5);  
}

TEST(ListTests, RemoveAt0_IntListOf5Values_CapacityUnaffected)
{
    List<int> testList(5);
    for (int i=0; i<5; ++i)
    {
        testList[i] = i;
    }
    
    testList.remove_at(0);
    
    EXPECT_EQ(testList.capacity(), 5);  
}

TEST(ListTests, RemoveAt0_IntListOf5Values_0thElementRemoved)
{
    List<int> testList(5);
    for (int i=0; i<5; ++i)
    {
        testList[i] = i;
    }
    
    testList.remove_at(0);
    
    EXPECT_EQ(testList[0], 1);  
}

TEST(ListTests, RemoveAt0_IntListOf5Values_SizeDecreases)
{
    List<int> testList(5);
    for (int i=0; i<5; ++i)
    {
        testList[i] = i;
    }
    
    testList.remove_at(0);
    
    EXPECT_EQ(testList.size(), 4);  
}

TEST(ListTests, ForeachLoop_IntListOf5Values_SumIs5)
{
    List<int> testList(5);
    for (int i=0; i<5; ++i)
    {
        testList[i] = 1;
    }

    int i=0;
    for (auto element : testList)
    {
        i += element;
    }
    
    EXPECT_EQ(i, 5);  
}

TEST(ListTests, Resizing_ResizeEmptyTo10_SizeIs10)
{
    List<int> testList;
    testList.resize(10);
    
    EXPECT_EQ(testList.size(), 10);  
}

TEST(ListTests, Resizing_Resize10To0_SizeIs0)
{
    List<int> testList(10);
    testList.resize(0);
    
    EXPECT_EQ(testList.size(), 0);  
}

TEST(ListTests, Empty_EmptyList_True)
{
    List<int> testList;
    
    EXPECT_TRUE(testList.empty());
}

TEST(ListTests, Empty_IntListOf5_False)
{
    List<int> testList(5);
    
    EXPECT_FALSE(testList.empty());
}

TEST(ListTests, Front_SequentialIntListOf5_FrontIs0)
{
    List<int> testList(5);

    for(int i = 0; i < testList.size(); ++i)
    {
        testList[i] = i;
    }
    
    EXPECT_EQ(testList.front(), 0);
}

TEST(ListTests, Back_SequentialIntListOf5_BackIs4)
{
    List<int> testList(5);

    for(int i = 0; i < testList.size(); ++i)
    {
        testList[i] = i;
    }
    
    EXPECT_EQ(testList.back(), 4);
}

TEST(ListTests, ShrinkToFit_Capacity10Size5_CapacityShrinksTo5)
{
    List<int> testList(5);
    testList.reserve(10);
    EXPECT_EQ(testList.capacity(), 10);

    testList.shrink_to_fit();    
    EXPECT_EQ(testList.capacity(), 5);
}

TEST(ListTests, ShrinkToFit_Capacity5Size5_CapacityUnaffected)
{
    List<int> testList(5);
    testList.shrink_to_fit();
    
    EXPECT_EQ(testList.capacity(), 5);
}

TEST(ListTests, PopBack_SequentialIntListOf5_LastElementIs3)
{
    List<int> testList(5);

    for(int i = 0; i < testList.size(); ++i)
    {
        testList[i] = i;
    }

    EXPECT_EQ(testList.size(), 5);
    
    testList.pop_back();
    
    EXPECT_EQ(testList.back(), 3);
}

TEST(ListTests, PopBack_SequentialIntListOf5_SizeReduced)
{
    List<int> testList(5);

    for(int i = 0; i < testList.size(); ++i)
    {
        testList[i] = i;
    }

    EXPECT_EQ(testList.size(), 5);
    
    testList.pop_back();
    
    EXPECT_EQ(testList.size(), 4);
}

TEST(ListTests, InsertValueAt3_IntListOf5_ValueInsertedAt3)
{
    List<int> testList(5);
    testList.insert(1, 3);

    EXPECT_EQ(testList[3], 1);
}

TEST(ListTests, InsertValueAt3_SequentialIntListOf5_Value4is3)
{
    List<int> testList(5);
    
    for(int i = 0; i < testList.size(); ++i)
    {
        testList[i] = i;
    }
    
    testList.insert(1, 3);

    EXPECT_EQ(testList[4], 3);
}

TEST(ListTests, Clear_IntListOf5_SizeIs0)
{
    List<int> testList(5);
    testList.clear();

    EXPECT_EQ(testList.size(), 0);
}