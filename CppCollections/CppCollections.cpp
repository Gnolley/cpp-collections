#include <iostream>
#include <vector>
#include "List.h"

using Collections::List;


int main()
{
	List<int> testList(5);
	for (int i=0; i<5; ++i)
	{
		testList[i] = i;
	}
	
	testList.remove_at(0);

	
	for (auto ls : testList)
	{
		std::cout << ls << " ";
	}

	return 0;
}
