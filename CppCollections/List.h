#pragma once
#include <algorithm>
#include <format>

namespace Collections
{
    struct Iterator;
    
    template<typename TItem>
    class List
    {
        
    public:

        List();
        List(size_t size);
        List(TItem* array, size_t size);
        List(const List& list);
        List(List&& list) noexcept;
        
        ~List();
        
        TItem& operator[](int index)
        {
            if(index >= currentSize)
            {
                throw std::runtime_error(std::format("Error accessing element {} of List. Index out of bounds!", index));
            }
            
            return data[index];
        }
        
        void reserve(size_t capacity);
        void resize(size_t size);
        void shrink_to_fit();
        void push_back(TItem item);
        void remove(TItem item);
        void remove_at(size_t index);
        void pop_back();
        void insert(TItem item, size_t position);
        void clear();
        
        [[nodiscard]] TItem& at(size_t index) const
        {
            if(index > currentSize - 1)
            {
                throw std::overflow_error(std::format("Cannot access value at index {}, index out of range!", index));
            }
            
            return data[index];
        }

        [[nodiscard]] TItem& front() const
        {
            return *data;
        }

        [[nodiscard]] TItem& back() const
        {
            return *(data + (currentSize - 1));
        }

        [[nodiscard]] size_t size() const
        {
            return currentSize;
        }

        [[nodiscard]] size_t capacity() const
        {
            return currentCapacity;
        }

        [[nodiscard]] bool empty() const
        {
            return currentSize == 0;
        }


        struct Iterator
        {
            using iterator_category = std::random_access_iterator_tag;
            using difference_type = std::ptrdiff_t;
            using value_type = TItem;
            using pointer = TItem*;
            using reference = TItem&;

        public:
            Iterator(pointer ptr) : _ptr(ptr) {}

            reference operator*() const { return *_ptr; }
            pointer operator->() { return _ptr; }

            Iterator& operator++()
            {
                ++_ptr;
                return *this;
            }

            Iterator operator++(TItem)
            {
                Iterator tmp = *this;
                ++*this;
                return tmp;
            }

            Iterator& operator--()
            {
                --_ptr;
                return *this;
            }

            Iterator operator--(TItem)
            {
                Iterator tmp = *this;
                --*this;
                return tmp;
            }

            friend bool operator==(const Iterator& a, const Iterator& b) { return a._ptr == b._ptr; }
            friend bool operator!=(const Iterator& a, const Iterator& b) { return a._ptr != b._ptr; }
            
        private:
            pointer _ptr;
        };

        Iterator begin()
        {
            return Iterator(data);
        }

        Iterator end()
        {
            return Iterator(data + (currentSize));
        }

    private:
        TItem* data = nullptr;
        size_t currentCapacity = 0;
        size_t currentSize = 0;

        static constexpr double CAPACITY_GROWTH_FACTOR = 1.5; 
        
        static size_t calculateCapacity(size_t requiredSize)
        {
            return static_cast<size_t>(static_cast<double>(requiredSize) * CAPACITY_GROWTH_FACTOR);
        }
    };

        template<typename TItem>
        List<TItem>::List()
        {
            currentCapacity = 0;
            currentSize = 0;
            data = nullptr;
        }
    
        template<typename TItem>
        List<TItem>::List(size_t size)
        {
            currentCapacity = size;
            currentSize = size;

            if (size > 0)
            {
                data = new TItem[size];
            }
        }
    
        template<typename TItem>
        List<TItem>::List(TItem* array, size_t size)
        {
            currentCapacity = size;
            currentSize = size;

            data = new TItem[size];
            std::copy(array, array + size, data); // probably doesn't work.
        }
    
        template<typename TItem>
        List<TItem>::List(const List& list)
        {
            if(list.currentCapacity > 0)
            {
                this->data = new TItem[list.currentCapacity];
                this->currentSize = list.currentSize;
            }

            if(list.currentSize > 0)
            {
                std::copy(list[0], list[0] + list.currentSize, data);
                this->currentCapacity = list.currentCapacity;
            }
        }
    
        template<typename TItem>
        List<TItem>::List(List&& list) noexcept
        {
            data = list.data;
            currentCapacity = list.currentCapacity;
            currentSize = list.currentSize;
            
            list.data = nullptr;
        }

        template<typename TItem>
        List<TItem>::~List()
        {
            if (data == nullptr) return;
            delete[] data;
        }

        template<typename TItem>
        void List<TItem>::reserve(size_t capacity)
        {
            if(data == nullptr)
            {
                data = new TItem[capacity];
                currentCapacity = capacity;
            }
            else if(capacity > currentCapacity)
            {                
                TItem* temp = new TItem[capacity];
                std::copy(data, data + currentSize, temp);
                
                delete[] data;
                data = temp;
                currentCapacity = capacity;
            }
        }

        template<typename TItem>
        void List<TItem>::resize(size_t size)
        {
            if(size == currentSize)
            {
                return;
            }

            if(data == nullptr)
            {
                data = new TItem[size];
                currentCapacity = size;
                currentSize = size;
            }
            else if(size > currentCapacity)
            {                
                TItem* temp = new TItem[size];
                std::copy(data, data + currentSize, temp);
                
                delete[] data;
                data = temp;
                currentCapacity = size;
            }

            currentSize = size;
        }

        template<typename TItem>
        void List<TItem>::shrink_to_fit()
        {
            if(currentSize == currentCapacity)
            {
                return;
            }

            TItem* temp = new TItem[currentSize];
            std::copy(data, data + currentSize, temp);
            delete data;
            data = temp;

            currentCapacity = currentSize;
        }
        
        template<typename TItem>
        void List<TItem>::push_back(TItem item)
        {
            if(currentCapacity == currentSize)
            {
                reserve(calculateCapacity(currentSize + 1));
            }

            data[currentSize] = item;
            ++currentSize;
        }
        
        template<typename TItem>
        void List<TItem>::remove(TItem item)
        {
            for (int i=0; i<currentSize; ++i)
            {
                if(data[i] == item)
                {
                    remove_at(i);
                    break;
                }
            }
        }

        template<typename TItem>
        void List<TItem>::remove_at(size_t index)
        {
            if(index >= currentSize)
            {
                throw std::runtime_error( std::format("Cannot remove value at index {}, index out of range!", index));
            }
            
            for (size_t i = index + 1; i < currentSize; ++i)
            {
                data[i - 1] = data[i];
            }
            
            data[--currentSize] = NULL;
        }

        template<typename TItem>
        void List<TItem>::pop_back()
        {
            data[currentSize - 1] = NULL;
            --currentSize;
        }

        template<typename TItem>
        void List<TItem>::insert(TItem item, size_t position)
        {
            if(position >= currentCapacity)
            {
                throw std::runtime_error(std::format("Cannot insert item a position {}! Index out of range.", position));
            }
            
            if(currentCapacity == currentSize)
            {
                reserve(calculateCapacity(currentSize + 1));
            }

            for(size_t i = currentSize-1; i >= position; --i)
            {
                data[i+1] = data[i];
            }
            data[position] = item;
            ++currentSize;
        }

        template<typename TItem>
        void List<TItem>::clear()
        {
            for(int i=0; i<currentSize; ++i)
            {
                data[i] = NULL;
            }
            currentSize = 0;
        }
}
